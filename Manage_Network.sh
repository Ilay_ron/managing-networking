#!/usr/bin/env bash

#purpose: managing network configaration
#date: 2/12/2020
#version: v1.0.0
################################################33



Main(){
Configare_persistance_address_with_net_mask
configure_ipv6
Print_all_interfaces_and_ips
Check_interface_and_ip_addresses
Find_my_linux_dist
Check_logs
}


Configare_persistance_address_with_net_mask(){

ifconfig enp0s3 10.0.2.16 netmask 255.255.255.0 

route add gw 10.10.10.1 enp0s3
echo "nameserver 8.8.8.8" > /etc/resolve.conf
}


Configure_ipv6(){

nmcli con add type ethernet con-name Network-Managing ifname enp0s3 ipv4 10.10.10/24 gw4 10.10.10.254 ip6 abbe::netManaging gw6 2001:db8::1
nmcli con up Network-managing
}

Print_all_interfaces_and_ips(){

ifconfig -a

}

Check_interface_and_ip_addresses(){

IF=$(ifconfig -a)

for $eth in IF;
do 
if [[ -z $eth ]]
then
nmcli con add type ethernet con-name NetManagment ifname eth0 ipv4 10.10.10.2/24 gw4 10.10.10.254

else
echo $eth
 
fi
done


}

Find_my_linux_dist(){

lsb_release -a

}

Check_logs(){

exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3 4
exec 1>log.out 2>&1
#everything will go to file log.out"
echo "$(date) : part1 - start" >&3 
}


Main
